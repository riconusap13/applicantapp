<?php
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\applicant;
use App\User;

Route::get('/test',function(){
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home')->middleware('role:Applicant');

Route::get('/admin', 'HomeController@adminHome')->name('admin')->middleware('role:Admin');

Route::get('/superadmin', 'HomeController@superadminHome')->name('superadmin')->middleware('role:Super Admin');
//Home
Route::get('/home', 'HomeController@index')->name('home');

route::get('/',function(){
    if(Auth::User()){
        return redirect()->route('home');
    }
    else{
        return redirect()->route('login');
    }
});

//Register
Route::view('/register-applicant', 'registerr')->name('register-form');
Route::post('/register/store','UserController@store')->name('register-applicant');

//Edit Profile
Route::get('/home/edit-profile/{user}', function(User $user){
    $applicants = new applicant;
    $applicants = applicant::where('user_id',$user->id)->first();
    return view('edit',['applicant' => $applicants]);
})->name('editprofile');

Route::put('/home/edit-profile/{applicant}', 'UserController@edit')->name('edit-profile');

//Download CV
Route::get('/home/edit-profile/download-pdf/{applicant}', function(applicant $applicant){
        $pdf = Pdf::loadView('pdf.export', compact('applicant'));
        
        return $pdf->download('Cv-Applicant.pdf');
})->name('download');



