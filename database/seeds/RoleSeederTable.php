<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Role;

class RoleSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement("SET FOREIGN_KEY_CHECKS=0");
        
        Role::truncate();

        $role = Role::create([
            'role_name' => 'Applicant',
        ]);

        $role = Role::create([
            'role_name' => 'Admin',
        ]);

        $role = Role::create([
            'role_name' => 'Super Admin',
        ]);

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
