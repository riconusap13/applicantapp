<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\User;


class UserSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        $irfan = User::create([
            'id' => 1,
            'name' => 'irfan',
            'email' => 'irfan@gmail.com',
            'password' => Hash::make('password'),
        ]);

        $admin = User::create([
            'id' => '2', // 'id' => '2
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('password'),
            'role' => '2',
        ]);

        $superadmin = User::create([
            'id' => '3', // 'id' => '3
            'name' => 'superadmin',
            'email' => 'superadmin@gmail.com',
            'password' => Hash::make('password'),
            'role' => '3',
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
