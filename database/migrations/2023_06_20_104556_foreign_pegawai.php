<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ForeignPegawai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create Foregin key for pegawai
        Schema::table('pegawai', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        // Drop Foregin key for pegawai
        Schema::table('pegawai', function (Blueprint $table) {
            $table->dropForeign('pegawai_user_id_foreign');
        });
    }
}
