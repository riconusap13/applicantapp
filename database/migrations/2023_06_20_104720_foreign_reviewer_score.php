<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ForeignReviewerScore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        // Create Foregin key for reviewer_score
        Schema::table('reviewer_score', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('applicant_id')->references('id')->on('applicants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop Foregin key for reviewer_score
        Schema::table('reviewer_score', function (Blueprint $table) {
            $table->dropForeign('reviewer_score_user_id_foreign');
            $table->dropForeign('reviewer_score_applicant_id_foreign');
        });
    }
}
