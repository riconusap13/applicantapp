<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Applicants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('applicants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('name', 255);
            $table->string('email', 255);
            $table->string('alamat', 255);
            $table->string('tempat_lahir', 100);
            $table->date('tgl_lahir');
            $table->string('status_perkawinan', 20);
            $table->string('posisi_yg_dilamar', 100);
            $table->string('status_penerimaan', 100)->nullable();
            $table->date('tgl_penerimaan')->nullable();
            $table->date('tgl_penolakan')->nullable();
            $table->string('photo_profile')->nullable();
            $table->string('about_me',500)->nullable();
            $table->timestamps();

            // $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('applicants');
    }
}
