@extends('layouts.template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content p-5">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 mx-auto">
                    <div class="col-sm-6">
                        <h1>Edit Profile</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                            <li class="breadcrumb-item active">Edit Profile</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <form method="POST" action="{{ route('edit-profile', ['applicant' => $applicant->id]) }}"
                enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="image-container mb-3" style="text-align: center;">
                    <a href="#" style="margin-bottom: 10px;">
                        @if (!empty($applicant->photo_profile))
                            <img class="img-fluid img-circle"
                                src="{{ asset('storage/profile_images/' . $applicant->photo_profile) }}" alt="Photo Profile"
                                style="border: 5px solid #666666; margin: 0 auto; padding: 3px; width: 150px; height: 150px;">
                        @else
                            <img class="img-fluid img-circle" src="{{ URL::asset('template/dist/img/user2-160x160.jpg') }}"
                                alt="Default Image"
                                style="border: 5px solid #666666; margin: 0 auto; padding: 3px; width: 150px;">
                        @endif
                    </a>
                    <br><br>
                    <div class="mb-3">
                        <input class="form-control form-control-sm" id="formFileSm" type="file" name="photo_profile" >
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">General</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="inputName">Nama</label>
                                    <input type="text" name="name" id="inputName" class="form-control"
                                        value="{{ $applicant->name }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail">Email</label>
                                    <input type="text" name="email" id="inputEmail" class="form-control"
                                        value="{{ $applicant->email }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputAlamat">Alamat</label>
                                    <input type="text" name="alamat" id="inputAlamat" class="form-control"
                                        value="{{ $applicant->alamat }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputTempatLahir">Tempat Lahir</label>
                                    <input type="text" name="tempat_lahir" id="inputTempatLahir" class="form-control"
                                        value="{{ $applicant->tempat_lahir }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputTglLahir">Tanggal Lahir</label>
                                    <input type="text" name="tgl_lahir" id="inputTglLahir" class="form-control"
                                        value="{{ $applicant->tgl_lahir }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputStatus">Status</label>
                                    <input type="text" name="status_perkawinan" id="inputStatus" class="form-control"
                                        value="{{ $applicant->status_perkawinan }} ">
                                </div>
                                <div class="form-group">
                                    <label for="inputPosisi">Posisi yang Dilamar</label>
                                    <input type="text" name="posisi_yg_dilamar" id="inputPosisi" class="form-control"
                                        value="{{ $applicant->posisi_yg_dilamar }}">
                                </div>
                                {{-- <div class="form-group">
                <label for="inputStatus">Status Perkawinan</label>
                <select id="inputStatus" class="form-control custom-select">
                  <option disabled>Select one</option>
                  <option>Belum Kawin</option>
                  <option selected>Kawin</option>
                </select>
              </div>
              <div class="form-group">
                <label for="inputPosisi">Posisi yang dilamar</label>
                <select id="inputPosisi" class="form-control custom-select">
                  <option disabled>Select one</option>
                  <option>Programmer</option>
                  <option selected>Analyst</option>
                </select>
              </div>               --}}
                                <div class="form-group">
                                    <label for="inputAboutMe">About Me</label>
                                    <textarea name="about_me" id="inputAboutMe" class="form-control" rows="5">{{ $applicant->about_me }}</textarea>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <a href="#" class="btn btn-secondary">Cancel</a>
                        <a href="{{ route('download', ['applicant' => $applicant->id]) }}" class="btn btn-primary">Unduh
                            PDF</a>
                        <button type="submit" class="btn btn-success float-right">Save Changes</button>
                    </div>
                </div>
            </form>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->
@endsection
