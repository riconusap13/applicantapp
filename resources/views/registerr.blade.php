<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Register Applicant</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="template/plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="template/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="template/dist/css/adminlte.min.css">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href=""><b>Kabayan</b>Group</a>
  </div>

  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg">Register Applicant</p>

      <form action="{{route('register-applicant')}}" method="post">
        @csrf
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="name" id="name" placeholder="Nama" value="{{old('name')}}">
          <div class="input-group-append">
            <div class="input-group-text">
              {{-- <span class="fas fa-user"></span> --}}
              <ion-icon name="person-outline"></ion-icon>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="{{old('email')}}">
          <div class="input-group-append">
            <div class="input-group-text">
              {{-- <span class="fas fa-envelope"></span> --}}
              <ion-icon name="lock-closed-outline"></ion-icon>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="{{('password')}}">
          <div class="input-group-append">
            <div class="input-group-text">
              {{-- <span class="fas fa-lock"></span> --}}
              <ion-icon name="lock-closed-outline"></ion-icon>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control"  name="alamat" id="alamat" placeholder="Alamat" value="{{old('alamat')}}">
          <div class="input-group-append">
            <div class="input-group-text">
              <ion-icon name="pin-outline"></ion-icon>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control"  name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir" value="{{old('tempat_lahir')}}">
          <div class="input-group-append">
            <div class="input-group-text">
              <ion-icon name="pin-outline"></ion-icon>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control"  name="tgl_lahir" id="tgl_lahir" placeholder="YYYY-MM-DD" value="{{old('tgl_lahir')}}">
          <div class="input-group-append">
            <div class="input-group-text">
              <ion-icon name="calendar-outline"></ion-icon>
            </div>
          </div>
        </div>
        
        <div class="input-group mb-3">
          <input type="text" class="form-control"  name="status_perkawinan" id="status_perkawinan" placeholder="Belum Kawin" value="{{old('status_perkawinan')}}">
          <div class="input-group-append">
            <div class="input-group-text">
              <ion-icon name="heart-circle-outline"></ion-icon>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control"  name="posisi_yg_dilamar" id="posisi_yg_dilamar" placeholder="Analyst" value="{{old('posisi_yg_dilamar')}}">
          <div class="input-group-append">
            <div class="input-group-text">
              <ion-icon name="bag-outline"></ion-icon>
            </div>
          </div>
        </div>
        
        <div class="row" style="justify-content: center">
          <!-- /.col -->
          <div class="col-4" >
            <button type="submit" class="btn btn-primary btn-block ">Register</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

     

      <a href="{{route('login')}}" class="text-center">I already have a account</a>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
<script src="template/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="template/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="template/dist/js/adminlte.min.js"></script>
<script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
</body>
</html>
