<!DOCTYPE html>
<html>
<head>
    <title>Data Applicant</title>
</head>
<body>
    <h1>Data Applicant</h1>
    <p>Nama: {{ $applicant->name }}</p>
    <p>Email: {{ $applicant->email }}</p>
    <p>Alamat: {{ $applicant->alamat }}</p>
    <p>Tempat Lahir: {{ $applicant->tempat_lahir }}</p>
    <p>Tanggal Lahir: {{ $applicant->tgl_lahir }}</p>
    <p>Status Perkawinan: {{ $applicant->status_perkawinan }}</p>
    <p>Posisi yang dilamar: {{ $applicant->posisi_yg_dilamar }}</p>
    <p>Tentang Saya: {{ $applicant->about_me }}</p>

    <!-- Tambahkan kolom lain yang ingin ditampilkan -->
</body>
</html>
