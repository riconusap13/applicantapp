@extends('layouts.template')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            {{-- <img class="profile-user-img img-fluid img-circle" src="{{ asset('storage/profile_photos/' . $applicant->photo_profile) }}"
                                alt="User profile picture" style="width: 100px; height: 100px;"> --}}
                                @if (!empty($applicant->photo_profile))
                                <img class="img-circle elevation-2"
                                    src="{{ asset('storage/profile_images/' . $applicant->photo_profile) }}"
                                    alt="Photo Profile" style="width: 100px; height: 100px;">
                            @else
                                <img class="mg-circle elevation-2"
                                    src="{{ URL::asset('template/dist/img/user2-160x160.jpg') }}" alt="Default Image">
                            @endif
                        </div>
                        <h3 class="profile-username text-center">{{$applicant->name}}</h3>
                        <br>
                        <table class="table text-center">
                            <tbody>
                              <tr>
                                <th class="text-start" scope="row">Email</th>
                                <td class="text-end">{{$applicant->email}}</td>
                              </tr>
                              <tr>
                                <th class="text-start" scope="row">alamat</th>
                                <td class="text-end">{{$applicant->alamat}}</td>
                              </tr>
                              <tr>
                                <th class="text-start" scope="row">Tempat Lahir</th>
                                <td class="text-end">{{$applicant->tempat_lahir}}</td>
                              </tr>
                              <tr>
                                <th class="text-start" scope="row">Tanggal</th>
                                <td class="text-end">{{$applicant->tgl_lahir}}</td>
                              </tr>
                              <tr>
                                <th class="text-start" scope="row">Status Perkawinan</th>
                                <td class="text-end">{{$applicant->status_perkawinan}}</td>
                              </tr>
                              <tr>
                                <th class="text-start" scope="row">Posisi yang dilamar</th>
                                <td class="text-end">{{$applicant->posisi_yg_dilamar}}</td>
                              </tr>
                              <tr>
                                <th class="text-start" scope="row">Tentang Saya</th>
                                <td class="text-end">{{$applicant->about_me}}</td>
                              </tr>
                            </tbody>
                          </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
