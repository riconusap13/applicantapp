<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reviewerScore extends Model
{
    //
    protected $fillable = [
        'user_id',
        'applicant_id',
        'score',
        'created_at',
        'updated_at'
    ];

    public function users()
    {
        return $this->belongsTo('App\User');
    }

    public function applicants()
    {
        return $this->belongsTo('App\Applicant');
    }
}
