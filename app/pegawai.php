<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pegawai extends Model
{
    //
    protected $fillable=[
        'name',
        'email',
        'alamat',
        'tempat_lahir',
        'tgl_lahir',
        'status_perkawinan',
        'posisi',
        'tmt',
        'created_at',
        'updated_at'
    ];

    public function users()
    {
        return $this->belongsTo('App\User');
    }

    public function applicants()
    {
        return $this->belongsTo('App\Applicant');
    }
}
