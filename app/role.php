<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class role extends Model
{
    //
    protected $fillable=[
        'role_name',
        'created_at',
        'updated_at'
    ];

    public function users()
    {
        return $this->belongsTo('App\User');
    }

    // public function check()
    // {
    //     return $this->belongsTo('App\User');
    // }
}
