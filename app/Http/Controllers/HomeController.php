<?php

namespace App\Http\Controllers;

use App\applicant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        // dd($user->id);
        $applicants = new applicant;
        $applicants = applicant::where('user_id',$user->id)->first(); 
        // dd($applicants);
        return view('home',['applicant' => $applicants]);
    }

    public function adminHome()
    {
        return view('admin.ViewAdmin');
    }

    public function superadminHome()
    {
        return view('superadmin.ViewSuperAdmin');
    }
}

