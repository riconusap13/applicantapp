<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\role;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public function redirectTo()
    {
        // User role
        $user = auth()->user();
        $userRole = [$user->role];
        $roleNames = role::whereIn('id', $userRole)->pluck('role_name')->toArray();

        // Check user role
        switch ($roleNames[0]) {
            case 'Admin':
                $redirectTo = RouteServiceProvider::ADMIN;
                break;
            case 'Super Admin':
                $redirectTo = RouteServiceProvider::SUPERADMIN;
                break;
            default:
                $redirectTo = RouteServiceProvider::HOME;
                break;
        }

        return $redirectTo;
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
