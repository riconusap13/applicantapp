<?php

namespace App\Http\Controllers;
use App\User;
use App\applicant;
use Illuminate\Auth\Events\Validated;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;


use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
        
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function registerApplicant(Request $request)
    // {
        
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email'=> $request->email,
            'password' =>  Hash::make($request->password)
        ]);
        $user->save();

        $applicant = applicant::create([
            'user_id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'alamat' => $request->alamat,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'status_perkawinan' => $request->status_perkawinan,
            'posisi_yg_dilamar' => $request->posisi_yg_dilamar,
            // 'status_penerimaan' => null,
            // 'tgl_penerimaan' => null,
            // 'tgl_penolakan' => null
        ]);
        $applicant->save();
        return redirect()->route('home')->with('success', 'Success! You are registered!');
    }

    public function edit(applicant $applicant, Request $request)
    {
        $data = $request->all();

    if ($request->hasFile('photo_profile')) {
        $request->validate([
            'photo_profile' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        $photo = $request->file('photo_profile');
        $filename = 'profile_' . $applicant->id . '.' . $photo->getClientOriginalExtension();

        // Simpan file di direktori "storage/app/public/profile_images"
        $path = $photo->storeAs('public/profile_images', $filename);

        // Buat symlink ke direktori "public/storage/profile_images"
        $symlinkPath = public_path('storage/profile_images');
        if (!is_dir($symlinkPath)) {
            // Jika symlink belum ada, buat symlink baru
            Artisan::call('storage:link');
        }

        // Pindahkan file dari direktori "storage/app/public/profile_images" ke symlink
        $publicPath = public_path('storage/profile_images/' . $filename);
        File::move(storage_path('app/' . $path), $publicPath);

        // Hapus foto lama jika ada
        if ($applicant->photo_profile) {
            Storage::delete('public/profile_images/' . $applicant->photo_profile);
        }

        $applicant->photo_profile = $filename;
    }



        // $applicant->user_id = $applicant->user_id;
        $applicant->name = $data['name'];
        $applicant->email = $data['email'];
        $applicant->alamat = $data['alamat'];
        $applicant->tempat_lahir = $data['tempat_lahir'];
        $applicant->tgl_lahir = $data['tgl_lahir'];
        $applicant->status_perkawinan = $data['status_perkawinan'];
        $applicant->posisi_yg_dilamar = $data['posisi_yg_dilamar'];
        $applicant->about_me = $data['about_me'];
        // dd($applicant);
        $applicant->save();

        return redirect()->route('home')
    ->with('success', 'Applicant updated successfully!');

    }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show($id)
    // {
    //     //
    // }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */


    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function update(Request $request, $id)
    // {
    //     //
    // }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy($id)
    // {
    //     //
    // }
}