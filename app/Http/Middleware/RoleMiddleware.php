<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        if ($request->user() === null) {
            return $next($request);
        }

        // Get the user role id
        $user = $request->user();
        $userRole = [$user->role];

        // Get the user role role_name
        $roleNames = Role::whereIn('id', $userRole)->pluck('role_name')->toArray();

        // Compare User Role with the ones that are allowed to access the route
        $result = strcmp($roleNames[0], $roles[0]);

        // If the user role is allowed, then continue with the request
        if ($result == 0) {
            return $next($request);
        }

        // If the user role is not allowed, then redirect to the route that is allowed for the user role
        $route = $this->redirectTo($roleNames[0]);
        
        return redirect($route);
    }

    public function redirectTo($route)
    {
        switch ($route) {
            case 'Admin':
                session()->flash('Error', 'Access Denied.');
                $redirectTo = RouteServiceProvider::ADMIN;
                break;
            case 'Super Admin':
                session()->flash('Error', 'Access Denied.');
                $redirectTo = RouteServiceProvider::SUPERADMIN;
                break;
            default:
                session()->flash('Error', 'Access Denied.');
                $redirectTo = RouteServiceProvider::HOME;
                break;
        }
        return $redirectTo;
    }
}
