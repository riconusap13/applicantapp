<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class applicant extends Model
{
    //
    protected $fillable = [
        'user_id',
        'name',
        'email',
        'alamat',
        'tempat_lahir',
        'tgl_lahir',
        'status_perkawinan',
        'posisi_yg_dilamar',
        'status_penerimaan',
        'tgl_penerimaan',
        'tgl_penolakan',
        'photo_profile',
        'about_me'
    ];
    public $timestamps = false;

    public function users()
    {
        return $this->belongsTo('App\User');
    }

    public function pegawais()
    {
        return $this->hasOne('App\pegawai');
    }

    public function reviewerScores()
    {
        return $this->hasMany('App\reviewerScore');
    }

}
